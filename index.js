/* eslint-disable no-undef */
const http = require("http");
const fs = require("fs");
var port = process.env.PORT || 3000;
const htmlFilePath = "./htmlFile.html";
const jsonFilePath = "./jsonFile.json";
const uuid = require('uuid');

function getFile(filePath) {
    try {
        const data = fs.readFileSync(filePath, "utf8");
        return data;
    } catch (err) {
        console.log(err);
        throw err;
    }
}

const server = http.createServer((req, res) => {
    console.log("new request made");

    const urlRecieved = (req.url);
    const arr = urlRecieved.split("/");

    console.log(arr);
    if (arr[arr.length - 2] == "status") {
        const yo = http.STATUS_CODES[arr[arr.length - 1]]
        res.end(`${yo} code returned`);
    }

    else if (arr[arr.length - 2] == "delay") {
        setTimeout(() => {
            res.end("200 code returned");
        }, arr[arr.length - 1]);
    }

    if (arr[arr.length - 1] == "") {
        res.end(getFile(htmlFilePath));
    }
    else if (arr[arr.length - 1] == "json") {
        res.end(getFile(jsonFilePath));
    }
    else if (arr[arr.length - 1] == "uuid") {
        console.log(uuid.v4());
        res.end(`{uuid : ${uuid.v4()}}`);
    }
    //         // case "/status/:lnkval":
})

server.listen(port, () => {
    console.log("server started on port " + port);
})