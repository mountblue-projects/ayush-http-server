module.exports = {
    "env": {
        "browser": true,
        "es2021": true
    },
    "overrides": [
        {
            "env": {
                "node": true
            },
            "files": [
                ".eslintrc.{js,cjs}"
            ],
            "parserOptions": {
                "sourceType": "script"
            }
        }
    ],
    "extends": ["eslint:recommended"],
    "parserOptions": {
        "ecmaVersion": "latest"
    },
    "rules": {
    }
}
